# RHC Sponsors #
Contributors: @erikdmitchell, @gabelloyd
Tags: sponsor
Requires at least: 4.4
Tested up to: 4.7.4
Stable tag: 0.3
Version: 0.3

Custom plugin for Red Hook Crit to display sponsors. Plugin comes with three templates, but new templates can be added within the theme if needed. Display of content via shortcodes.

## Description ##

Plugin does the following:
1. Creates a custom post type called "Sponsors"
2. Adds custom taxonomy of "Sponsor Levels" to custom post type.
3. Comes with default templates for carousel, page and single-sponsor.

There are no options for this plugin.

To order our sponsor levels: https://wordpress.org/plugins/taxonomy-terms-order/installation/
That plugin author is NSP-CODE. This plugin was built and tested with version 1.4.7

## Installation ##

1. Upload `rhc-sponsorss-plugin` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use shortcodes [ltc-sponsors] and [ltc-sponsors display='carousel'] to retrieve content. No template customizations are required.

## Frequently Asked Questions ##

# What are the shortcodes #

Standard: 

[rhc-sponsors]

Get Carousel:

[rhc-sponsors display='carousel']

# How do I order the sponsors? #
Install the plugin called "Category Order and Taxonomy Terms Order" by NSP-CODE. Then in the admin area navigate to Sponsors > Taxonomy Order. Drag and drop the preferred order. This will update your /sponsors page display.

# What are the shortcodes? #
`[ltc-sponsors]`, `[ltc-sponsors display='carousel']`, `[ltc-sponsors display='inline']`

# How do I display a carousel? #
In the WYSIWYG editor on a page, enter this shortcode: `[ltc-sponsors display='inline']`.

# Can I customize the display of the carousel? #
At this time, we have to customize the display within the jquery in the plugin. That is located at /js/bxslider_custom.js

# How do I customize the layout of the sponsors page? # 
The recommended method is to create a folder in your theme called "ltc-sponsorss-plugin/templates" then copy the desired template you want to this folder. The available template over rides are 'carousel.php', 'page-sponsors.php' and 'single-sponsor.php'

## Screenshots ##

1. screenshot-1.jpg
2. screenshot-2.jpg
3. screenshot-3.jpg

## Changelog ##

= 0.3.1 April 22, 2017 =
* Add ACF metabox to allow Gabe to get the sponsor website data. Info is called in `archive-ltc-sponsors` template located in the theme at this point.

= 0.3, April 22, 2017 =
* Clean up templates being called within plugin
* Remove the Levels sorting from admin
* Remove the archive-ltc-sponsors.php file from plugin in favor of putting into theme
* TODO: Use updated templating logic to allow for archive-ltc-sponsors to be included in this plugin 

= 0.2.9.2, November 27, 2016 =
* Remove templating from line 48 in functions file

= 0.2.9.1 =
* Remove singleColumn css to rely on theme styling for this

= 0.2.9 =
* Quick fix to styling of partners logos.

= 0.2.8 =
* Remove extra class in page-sponsors.scss file

= 0.2.6 =
* Change thumbnail size for sponsor-archive to 255xumlimited

= 0.2.5 =
* EDM created functions for controlling the display-inline template
* JGL styled display-inline and page-sponsors/archive-sponsors

= 0.2.4 =
* Add three column layout to page-sponsors
* Add singleColumn class to project
* Adjust max height/width of images on page-project


= 0.2.3 =
* Change size of sponsors logos on page-sponsors

= 0.2.2 =
* Create display-inline properties.

= 0.1.1 =
* Make a Compass project. Install SCSS files

= 0.1.0 =
* Initial Commit. Ships with formatted templates.