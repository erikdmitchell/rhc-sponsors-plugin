<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.
/**
 *
 */
function rhc_sponsors_scripts_styles() {
	wp_enqueue_style('rhc-sponsors-style',plugins_url('/css/rhc-sponsor-styles.css', __FILE__), array(), '0.3', 'screen' );
}
add_action('wp_enqueue_scripts','rhc_sponsors_scripts_styles');

/**
 * rhc_sponsor_get_template_part_return function.
 *
 * @access public
 * @param mixed $slug
 * @param mixed $name (default: null)
 * @param bool $load (default: true)
 * @return void
 */
// function rhc_sponsors_get_template_part_return($slug,$name=null,$load=true) {
// 	$template_loader = new RHC_Sponsors_Template_Loader;

// 	ob_start();
// 	$template_loader->get_template_part($slug,$name,$load);
// 	return ob_get_clean();
// }

/**
 * rhc_sponsor_template_check function.
 * https://code.tutsplus.com/articles/plugin-templating-within-wordpress--wp-31088
 *
 * @access public
 * @param mixed $template
 * @return void
 * @since 0.3
 * @author Gabe Lloyd
 */
function rhc_sponsors_template_check($template) {
	
	// Post ID
    $post_id = get_the_ID();
 
    // For all other CPT
    if ( get_post_type( $post_id ) != 'ltc-sponsors' ) {
        return $template;
    }

    // Templates called via Gamjo 
	$rhc_sponsors_template_loader = new RHC_Sponsors_Template_Loader;

	// if wp can't find an 'ltc-sponsors' template, use the plugin //
	if (is_singular()):
		$template = $rhc_sponsors_template_loader->get_template_part('single','sponsor');
	// elseif (is_archive()):
	// 	$template = $rhc_sponsors_template_loader->get_template_part('display','archive-grid');
	elseif (is_page()):
	 	$template = $rhc_sponsors_template_loader->get_template_part('content','grid');
	endif;

	return $template;
}
add_filter('template_include','rhc_sponsors_template_check');

/**
 * get_rhc_sponsors function.
 *
 * @access public
 * @return void
 */
function get_rhc_sponsors() {
	$taxonomies=get_terms('sponsor-level');

	// no levels, bail //
	if (empty($taxonomies))
		return false;

	// append posts (sponsors) //
	foreach ($taxonomies as $tax) :
		$args = array(
			'post_type' => 'ltc-sponsors',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby'   => 'menu_order',
			'order'     => 'ASC',
			'sponsor-level' => $tax->slug
		);
		$tax->posts=get_posts($args);
	endforeach;

	return $taxonomies;
}

/**
 * get_rhc_archive_sponsors function.
 *
 * We want to run a standard query to get all sponsors that have been checked to display on the homepage
 *
 * @access public
 * @return void
 */
function get_rhc_archive_sponsors() {

	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'ltc-sponsors' ),
		'post_status'            => array( 'publish' ),
		'order'                  => 'ASC',
		'orderby'                => 'menu_order',
		'tax_query'              => array(
			array(
				'taxonomy'         => 'sponsor-level', // has to be published and assigned to a sponsor-level
				'field'            => 'slug',
				'terms'			=> $sponsor_level->slug,
			),
		),
	);

	// The Query
	$sponsors_level_query = new WP_Query( $args );

	if (!count($sponsors_level_query->posts))
		return false;

	return $sponsors_level_query;

	// Restore original Post Data
	wp_reset_postdata();
}

/**
 * get_rhc_homepage_sponsors function.
 *
 * We want to run a standard query to get all sponsors that have been checked to display on the homepage
 *
 * @access public
 * @return void
 */
function get_rhc_homepage_sponsors() {

	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'ltc-sponsors' ),
		'post_status'            => array( 'publish' ),
		'order'                  => 'ASC',
		'orderby'                => 'menu_order',
		'meta_query'             => array(
			array(
				'key'       => 'display_on_homepage', //this is the ACF checkbox field
				'value'     => 1, // if it's TRUE, we show it on the homepage
				'compare'   => '=', // this is the qualifier
			),
		),
	);

	// The Query
	$sponsors_query = new WP_Query( $args );

	if (!count($sponsors_query->posts))
		return false;

	return $sponsors_query;

// Restore original Post Data
wp_reset_postdata();
}

/**
 * rhc_sponsors_page_sponsor_website function.
 *
 * @access public
 * @param int $sponsor_id (default: 0)
 * @return void
 */
function rhc_sponsors_page_sponsor_website($sponsor_id=0) {

	if (get_post_meta($sponsor_id,'_sponsor_website',true) && get_post_meta($sponsor_id,'_sponsor_website',true)!='') :
		$website=get_post_meta($sponsor_id,'_sponsor_website',true);
	else :
		$website=get_permalink($sponsor_id);
	endif;

	echo $website;
}

/**
 * rhc_sponsors_page_sponsor_title function.
 *
 * @access public
 * @param int $sponsor_id (default: 0)
 * @return void
 */
function rhc_sponsors_page_sponsor_title($sponsor_id=0) {
	if (get_post_meta($sponsor_id,'_sponsor_website',true) && get_post_meta($sponsor_id,'_sponsor_website',true)!='') :
		$title=get_the_title($sponsor_id);
	else :
		$title=the_title_attribute(array('echo' => false));
		
	endif;

	echo $title;
}

/**
 * rhc_sponsors_page_sponsor_thumbnail function.
 *
 * @access public
 * @param int $sponsor_id (default: 0)
 * @return void
 */
function rhc_sponsors_page_sponsor_thumbnail($sponsor_id=0) {
	if (has_post_thumbnail($sponsor_id)) :
		$thumbnail=get_the_post_thumbnail($sponsor_id,'sponsor-archive');
	else :
		$thumbnail=get_the_title($sponsor_id);
	endif;

	echo $thumbnail;
}



/**
 * rhc_sponsors_get_slug function.
 * We want to get the slug of each sponsor to target styling when flexbox is not supported.
 * @access public
 * @param int $sponsor_id (default: 0)
 * @return void
 */
function rhc_sponsors_get_slug($sponsor_id=0) {

	$post = get_post($sponsor_id);
	$slug = $post->post_name;

	echo $slug;
}

/**
 * ACF Sponsor Details Logic 
 * @since 0.3
 * @author Gabe Lloyd
 */
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_58fb9e3c40666',
	'title' => 'Sponsor Details',
	'fields' => array (
		array (
			'key' => 'field_58fb9e47ae6b3',
			'label' => 'Sponsor Website',
			'name' => 'sponsor_website',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'ltc-sponsors',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>