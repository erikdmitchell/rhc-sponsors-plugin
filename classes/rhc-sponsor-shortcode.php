<?php
/**
 * shortcode_rhc_sponsor function.
 * REf: https://pippinsplugins.com/template-file-loaders-plugins/
 *
 * @access public
 * @param mixed $atts
 * @return void
 */
function shortcode_rhc_sponsor($atts) {
	
	$atts=shortcode_atts(array(
		'display' => 'page' // page, carousel
	),$atts);

	ob_start();

	// Templates called via Gamjo 
	$rhc_sponsors_template_loader = new RHC_Sponsors_Template_Loader;

	
	switch ($atts['display']) :
		case 'page':
			$content = $rhc_sponsors_template_loader->get_template_part('content','grid');
			break;
		case 'inline' :
			$content = $rhc_sponsors_template_loader->get_template_part('content','inline');
			break;
		default:
			$content = $rhc_sponsors_template_loader->get_template_part('content','grid');
	endswitch;

	return $content;
	return ob_get_clean();
}
add_shortcode('rhc-sponsor','shortcode_rhc_sponsor');

function shortcode_sponsor_inline() {

	ob_start();

	// Templates called via Gamjo 
	$rhc_sponsors_template_loader = new RHC_Sponsors_Template_Loader;

	
	$rhc_sponsors_template_loader->get_template_part('content','inline');
			
	return ob_get_clean();
}
add_shortcode('rhc_sponsors_inline','shortcode_sponsor_inline');