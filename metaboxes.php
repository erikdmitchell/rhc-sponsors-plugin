<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.
/**
 * RHCSponsorDetails class.
 */
class RHCSponsorDetails {

	protected $inputs=array(
		'_sponsor_website',
		'_sponsor_facebook',
		'_sponsor_twitter',
		'_sponsor_instagram',
		'_sponsor_hashtag',
		'_sponsor_display_rhc_hash'
	);

  /**
   * Hook into the appropriate actions when the class is constructed.
   */
  public function __construct() {
		add_action('add_meta_boxes',array( $this,'add_meta_box'));
		add_action('save_post', array($this,'save'));
		add_action('admin_enqueue_scripts',array($this,'admin_scripts_styles'));

    add_action( 'after_setup_theme', array( $this, 'ensure_post_thumbnails_support' ) );
    add_action( 'after_theme_setup', array( $this, 'register_image_sizes' ) );
  }

  /**
   * admin_scripts_styles function.
   *
   * @access public
   * @param mixed $hook
   * @return void
   */
  public function admin_scripts_styles($hook) {
  	wp_enqueue_style('font-awesome-style',plugin_dir_url(__FILE__).'css/font-awesome.min.css',array(),'4.5.0');
  	wp_enqueue_style('rhc-sponsors-admin-metabox-style',plugin_dir_url(__FILE__).'css/admin-metabox.css');
  }

  /**
   * Adds the meta box container.
   */
  public function add_meta_box( $post_type ) {
      // Limit meta box to certain post types.
      $post_types = array('ltc-sponsors');

      if ( in_array( $post_type, $post_types ) ) {
					add_meta_box(
					'rhc_sponsor_details',
					__( 'Sponsor Details', 'rhc-sponsors' ),
					array( $this, 'render_meta_box_content' ),
					$post_type,
					'advanced',
					'high'
				);
      }
  }

  /**
   * Render Meta Box content.
   *
   * @param WP_Post $post The post object.
   */
  public function render_meta_box_content( $post ) {
    // Add an nonce field so we can check for it later.
    wp_nonce_field( 'update', 'rhc_sponsors_meta_box' );

    // Use get_post_meta to retrieve values from the database //
    $input_values=array();
		foreach ($this->inputs as $name) :
			$input_values[$name]=get_post_meta($post->ID,$name,true);
		endforeach;

		extract($input_values);

		// for our checkbox //
		if (empty($_sponsor_display_rhc_hash) || $_sponsor_display_rhc_hash=='')
			$_sponsor_display_rhc_hash=0;

    // Display the form, using the current value.
    ?>
    <div class="sponsor-details-row">
      <label for="sponsor_website">
        <?php _e('Sponsor Website', 'rhc-sponsors' ); ?>
        <div class="sponsor-input-wrap icon">
	        <i class="fa fa-globe"></i>
					<input type="text" id="sponsor_website" name="_sponsor_website" value="<?php echo esc_attr( $_sponsor_website ); ?>" class="full-width" />
        </div>
      </label>
    </div>
    <div class="sponsor-details-row">
      <label for="sponsor_facebook">
        <?php _e('Facebook URL', 'rhc-sponsors' ); ?>
        <div class="sponsor-input-wrap icon">
	        <i class="fa fa-globe"></i>
					<input type="text" id="sponsor_facebook" name="_sponsor_facebook" value="<?php echo esc_attr( $_sponsor_facebook ); ?>" class="full-width" />
        </div>
      </label>
    </div>
    <div class="sponsor-details-row">
      <label for="sponsor_twitter">
        <?php _e('Twitter URL', 'rhc-sponsors' ); ?>
        <div class="sponsor-input-wrap icon">
	        <i class="fa fa-globe"></i>
					<input type="text" id="sponsor_twitter" name="_sponsor_twitter" value="<?php echo esc_attr( $_sponsor_twitter ); ?>" class="full-width" />
        </div>
      </label>
    </div>
    <div class="sponsor-details-row">
      <label for="sponsor_instagram">
        <?php _e('Instagram URL', 'rhc-sponsors' ); ?>
        <div class="sponsor-input-wrap icon">
	        <i class="fa fa-globe"></i>
					<input type="text" id="sponsor_instagram" name="_sponsor_instagram" value="<?php echo esc_attr( $_sponsor_instagram ); ?>" class="full-width" />
        </div>
      </label>
    </div>
    <div class="sponsor-details-row">
      <label for="sponsor_hashtag">
        <?php _e('Specific Hashtag', 'rhc-sponsors' ); ?>
				<div class="description">Hashtag we use to mention this sponsor on our social media channels.</div>
				<input type="text" id="sponsor_hashtag" name="_sponsor_hashtag" value="<?php echo esc_attr( $_sponsor_hashtag ); ?>" class="full-width" placeholder="#rockstargames" />
      </label>
    </div>
    <div class="sponsor-details-row">
      <label for="sponsor_display_rhc_hash">
        <?php _e('Display #redhookcrit', 'rhc-sponsors' ); ?>
      </label>
			<div class="description">Should we display their social media posts on our website if they include #redhookcrit in the post?</div>
			<fieldset>
				<label title="yes"><input type="radio" name="_sponsor_display_rhc_hash" value="1" <?php checked($_sponsor_display_rhc_hash,1,true); ?> /> Yes</label>
				<label title="no"><input type="radio" name="_sponsor_display_rhc_hash" value="0" <?php checked($_sponsor_display_rhc_hash,0,true); ?> /> No</label>
			</fieldset>
    </div>
    <?php
  }

  /**
   * Save the meta when the post is saved.
   *
   * @param int $post_id The ID of the post being saved.
   */
  public function save( $post_id ) {
    /*
     * We need to verify this came from the our screen and with proper authorization,
     * because save_post can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['rhc_sponsors_meta_box'] ) ) {
        return $post_id;
    }

    $nonce = $_POST['rhc_sponsors_meta_box'];

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, 'update' ) ) {
        return $post_id;
    }

    /*
     * If this is an autosave, our form has not been submitted,
     * so we don't want to do anything.
     */
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }

    // Check the user's permissions.
    if ( 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Sanitize the user input //
    $input_post_values=array();
		foreach ($this->inputs as $name) :
			$input_post_values[$name]=sanitize_text_field($_POST[$name]);
		endforeach;

		// update meta fields //
		foreach ($input_post_values as $meta_name => $meta_value) :
			update_post_meta($post_id,$meta_name,$meta_value);
		endforeach;
  }
}

/**
 * Calls the class on the post edit screen.
 */
function call_RHCSponsorDetails() {
    new RHCSponsorDetails();
}

if ( is_admin() ) {
    add_action( 'load-post.php',     'call_RHCSponsorDetails' );
    add_action( 'load-post-new.php', 'call_RHCSponsorDetails' );
}

// ACF Fields to display an image on the homepage

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
  'key' => 'group_56fb39b6bcac5',
  'title' => 'Sponsors Homepage Image',
  'fields' => array (
    array (
      'key' => 'field_56fb37cc7cd7e', // need this value for the get_rhc_homepage_sponsors() function
      'label' => 'Display on homepage?',
      'name' => 'display_on_homepage',
      'type' => 'true_false',
      'instructions' => 'Choose if you want this sponsor to display on the homepage',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => 50,
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_56fb37ff7cd7f',
      'label' => 'Homepage Image',
      'name' => 'homepage_image',
      'type' => 'image',
      'instructions' => 'Should be all white. ',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => 50,
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => 1,
      'mime_types' => '',
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'ltc-sponsors',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'side',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

endif;

?>