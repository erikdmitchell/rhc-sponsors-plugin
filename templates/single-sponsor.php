<?php
/**
 * The template for displaying single sponsor posts.
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<header>
				<?php the_title( '<h1 class="entry-title sr-only">', '</h1>' ); 
			if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it.
				the_post_thumbnail('sponsor-archive', array( 'class' => 'center-block' ));
			else :
				the_title( '<h1 class="entry-title">', '</h1>' );
			endif; ?>
			</header>
			
			<div class="entry-content">
			<?php
				if($post->post_content=="") : //if the_content is blank, don't show it.
				else :
					the_content();
				endif;
				// start the custom layout options
			?>
			</div><!-- .entry-content -->
			<?php
				the_post_navigation();
			?>
		<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'red-hook-crit' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
			</footer><!-- .entry-footer -->
		</article><!-- #post-## -->
		<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>