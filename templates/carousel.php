<?php $sponsors=get_rhc_sponsors(); ?>

<div class="sponsor-slides" id="sponsor-slides">
	<ul class="sponsor-slider list-unstyled">
		<?php foreach ($sponsors as $group) : ?>
			<?php foreach ($group->posts as $sponsor): ?>
				<li class="sponsor-carousel-item">
					<a href="<?php echo get_permalink($sponsor->ID); ?>" alt="<?php echo $sponsor->post_title; ?>"><?php rhc_sponsors_page_sponsor_thumbnail($sponsor->ID); ?></a>
				</li>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</ul> <!-- /ul.sponsor-slider -->
</div>

