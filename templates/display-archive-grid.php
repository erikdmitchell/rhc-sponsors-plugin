<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package RHC_Sponsor_Plugin
 */

get_header(); ?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main" role="main">

		<header id="sponsor-header">
			<?php
				post_type_archive_title( '<h1 class="page-title screen-reader-text">', '</h1>' );
			?>
		</header><!-- .page-header -->

		<?php
		global $sponsor_level;

		// https://wordpress.stackexchange.com/questions/66219/list-all-posts-in-custom-post-type-by-taxonomy
		$sponsor_levels = get_terms('sponsor-level');

		if (empty($sponsor_levels))
		 		return false;

		foreach ($sponsor_levels as $sponsor_level) : setup_postdata($sponsor_level);
			
			// WP_Query arguments
			$args = array(
				'post_type'              => array( 'ltc-sponsors' ),
				'post_status'            => array( 'publish' ),
				// 'nopaging'               => true,
				// 'posts_per_page'         => '-1',
				// 'posts_per_archive_page' => '-1',
				'order'                  => 'ASC',
				'orderby'                => 'menu_order',
				'tax_query'              => array(
					array(
						'taxonomy'         => 'sponsor-level', // has to be published and assigned to a sponsor-level
						'field'            => 'slug',
						'terms'			=> $sponsor_level->slug,
					),
				),
			);
		

			// The Query
			$sponsor_level_query = new WP_Query( $args );

				// The Loop
				if ( $sponsor_level_query->have_posts() ) : ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<section class="group-wrap <?php echo $sponsor_level->slug;?>">
							<h2 class="sponsors-title"><?php echo $sponsor_level->name;?></h2>
							<ul class="sponsors-group">
							<?php while ( $sponsor_level_query->have_posts() ) : $sponsor_level_query->the_post(); ?>
								<li class="sponsors-item" id="<?php rhc_sponsors_get_slug($sponsor_level->ID); ?>">
									<a href="<?php rhc_sponsors_page_sponsor_website($sponsor_level->ID); ?>" alt="<?php rhc_sponsors_page_sponsor_title($sponsor_level->ID); ?>" target="_blank"><?php rhc_sponsors_page_sponsor_thumbnail($sponsor_level->ID); ?></a>
								</li>
							<?php endwhile; // endwhile 
							//RESET YOUR QUERY VARS
							wp_reset_query();?>
							</ul>
						</section>
					</article>
					
					<?php // the_posts_navigation(); Don't want this to display for this temmplate
					
				endif; //endif;

			
		
			endforeach; // end of the foreach loop 

			// Restore original Post Data
			wp_reset_postdata();
		
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();