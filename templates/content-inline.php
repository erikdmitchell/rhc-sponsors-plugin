<?php

$sponsors=get_rhc_homepage_sponsors(); //this is the loop

echo '<ul layout="rows center-spread" id="sponsors-inline">';
	foreach ($sponsors->posts as $sponsor): ?>
		<li class="<?php echo $sponsor->post_name ?>">
			<a href="<?php echo site_url().'/sponsors';?> "><img src="<?php echo wp_get_attachment_image_url(get_post_meta($sponsor->ID, 'homepage_image', true), 'sponsor-inline'); ?>" alt="<?php echo $sponsor->post_title; ?>"></a>
		</li>
	<?php endforeach;
echo '</ul>';