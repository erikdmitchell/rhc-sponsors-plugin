<?php
/**
 * The template for displaying sponsor pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package RHC_Sponsor_Plugin
 */

?>

<?php
// Start the Loop 
// This is our array. Find it in functions line 57

$sponsors=get_rhc_sponsors(); ?>

<article id="post-<?php the_ID(); ?> sponsors" <?php post_class(); ?>>
	<?php foreach ($sponsors as $group) : ?>
		<section class="group-wrap <?php echo $group->slug; ?>">
			<h2 class="sponsors-title"><?php echo $group->name; ?></h2>
			<ul class="sponsors-group">
				<?php foreach ($group->posts as $sponsor) : ?>
					<li class="sponsors-item">
						<a href="<?php rhc_sponsors_page_sponsor_website($sponsor->ID); ?>" alt="<?php rhc_sponsors_page_sponsor_title($sponsor->ID); ?>" target="_blank"><?php rhc_sponsors_page_sponsor_thumbnail($sponsor->ID); ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</section>
	<?php endforeach; ?>
</article>

<?php // Leave sidebar() and footer() out of this. Theme will use the above within the content area ?>