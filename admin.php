<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.

// Register Manage Columns
// ref: https://github.com/bamadesigner/manage-wordpress-posts-using-bulk-edit-and-quick-edit/blob/master/manage_wordpress_posts_using_bulk_edit_and_quick_edit.php

/*
 * Add post columns.
 * There are 3 different column filters: 'manage_pages_columns' for pages,
 * 'manage_posts_columns' which covers ALL post types (including custom post types),
 * and 'manage_{$post_type_name}_posts_columns' which only covers, you guessed it,
 * the columns for the defined $post_type_name.
 */

add_filter( 'manage_posts_columns', 'ltc_manage_posts_columns', 10, 2 );
function ltc_manage_posts_columns( $columns, $post_type ) {
	/**
	 * Add our new column after Title.
	 * Notice that we're specifying a post type because our function covers ALL post types.
	 *
	*/
	switch ( $post_type ) {

		case 'ltc-sponsors':

			// building a new array of column data
			$new_columns = array();

			foreach( $columns as $key => $value ) {

				// default-ly add every original column
				$new_columns[ $key ] = $value;

				/**
				 * If currently adding the title column,
				 * follow immediately with our custom columns.
				 */
				if ( $key == 'title' ) {
					$new_columns[ 'sponsor_level_column' ] = 'Level';

				}

			}

			return $new_columns;

	}

	return $columns;

}


// Make Our Taxonomy Sortable in Admin UI

add_filter( 'manage_edit-ltc-sponsors_sortable_columns', 'ltc_manage_sortable_columns' );
function ltc_manage_sortable_columns( $sortable_columns ) {

   /**
    * In this scenario, I already have a column with an
    * ID (or index) of 'sponsor_level_column'. Both column
    * indexes MUST match.
    *
    * The value of the array item (after the =) is the
    * identifier of the column data. For example, my
    * column data, 'release_date', is a custom field
    * with a meta key of 'release_date' so my
    * identifier is 'release_date'.
    */
   $sortable_columns[ 'sponsor_level_column' ] = 'sponsor-level';
   return $sortable_columns;

}

/**
 * Now that we have a column, we need to fill our column with data.
 * The filters to populate your custom column are pretty similar to the ones
 * that added your column: 'manage_pages_custom_column', 'manage_posts_custom_column',
 * and 'manage_{$post_type_name}_posts_custom_column'. All three pass the same
 * 2 arguments: $column_name (a string) and the $post_id (an integer).
 *
 * Our custom column data is post meta so it will be a pretty simple case of retrieving
 * the post meta with the meta key 'release_date'.
 *
 * Note that we are wrapping our post meta in a div with an id of �release_date-� plus the post id.
 * This will come in handy when we are populating our �Quick Edit� row.
 */
add_action( 'manage_posts_custom_column', 'ltc_populating_posts_custom_column', 10, 2 );
function ltc_populating_posts_custom_column( $column_name, $post_id ) {

	switch( $column_name ) {

		case 'sponsor_level_column':
			$term_list=wp_get_post_terms($post_id,'sponsor-level',array("fields" => "names"));

			echo '<div id="sponsor-level-' . $post_id . '">' . implode(', ',$term_list) . '</div>';
			break;



	}

}
/**
 * Just because we've made the column sortable doesn't
 * mean the posts will sort by our column data. That's where
 * this next 2 filters come into play.
 *
 * If your sort data is simple, i.e. alphabetically or numerically,
 * then 'pre_get_posts' is the filter to use. This filter lets you
 * change up the query before it's run.
 *
 * If your orderby data is more complicated, like our release date
 * which is a date string stored in a custom field, then check out
 * the 'posts_clauses' filter example used below.
 *
 * In the example below, when the main query is trying to order by
 * the 'film_rating', it's a simple alphabetical sorting by a custom
 * field so we're telling the query to set our 'meta_key' which is
 * 'film_rating' and that we want to order by the query by the
 * custom field's meta_value, e.g. PG, PG-13, R, etc.
 *
 * Check out http://codex.wordpress.org/Class_Reference/WP_Query
 * for more info on WP Query parameters.
 */
add_action( 'pre_get_posts', 'manage_ltc_pre_get_posts', 1 );
function manage_ltc_pre_get_posts( $query ) {
	/**
	 * We only want our code to run in the main WP query
	 * AND if an orderby query variable is designated.
	 */
	if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {

		switch( $orderby ) {

			// If we're ordering by 'sponsor-level'
			case 'sponsor-level':

				// set our query's meta_key, which is used for custom fields
				$query->set( 'meta_key', 'sponsor-level' );


				/**
				 * Tell the query to order by our custom field/meta_key's
				 * value, in this case: PG, PG-13, R, etc.
				 *
				 * If your meta value are numbers, change
				 * 'meta_value' to 'meta_value_num'.
				 */
				$query->set( 'orderby', 'meta_value' );

				break;

		}

	}

}

/**
 * Just because we've made the column sortable doesn't
 * mean the posts will sort by our column data. That's where
 * the filter above, 'pre_get_posts', and the filter below,
 * 'posts_clauses', come into play.
 *
 * If your sort data is simple, i.e. alphabetically or numerically,
 * then check out the 'pre_get_posts' filter used above.
 *
 * If your orderby data is more complicated, like combining
 * several values or a date string stored in a custom field,
 * then the 'posts_clauses' filter used below is for you.
 * The 'posts_clauses' filter allows you to manually tweak
 * the query clauses in order to sort the posts by your
 * custom column data.
 *
 * The reason more complicated sorts will not with the
 * "out of the box" WP Query is because the WP Query orderby
 * parameter will only order alphabetically and numerically.
 *
 * Usually I would recommend simply using the 'pre_get_posts'
 * and altering the WP Query itself but because our custom
 * field is a date, we have to manually set the query to
 * order our posts by a date.
 */
// add_filter( 'posts_clauses', 'manage_wp_posts_be_qe_posts_clauses', 1, 2 );
// function manage_wp_posts_be_qe_posts_clauses( $pieces, $query ) {
// 	global $wpdb;

// 	/**
// 	 * We only want our code to run in the main WP query
// 	 * AND if an orderby query variable is designated.
// 	 */
// 	if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {

// 		// Get the order query variable - ASC or DESC
// 		$order = strtoupper( $query->get( 'order' ) );

// 		// Make sure the order setting qualifies. If not, set default as ASC
// 		if ( ! in_array( $order, array( 'ASC', 'DESC' ) ) )
// 			$order = 'ASC';

// 		switch( $orderby ) {

// 			// If we're ordering by release_date
// 			case 'sponsor-level':

// 				/**
// 				 * We have to join the postmeta table to include
// 				 * our release date in the query.
// 				 */
// 				$pieces[ 'join' ] .= " LEFT JOIN $wpdb->postmeta wp_rd ON wp_rd.post_id = {$wpdb->posts}.ID AND wp_rd.meta_key = 'sponsor-level'";

// 				// Then tell the query to order by our date
// 				// $pieces[ 'orderby' ] = "STR_TO_DATE( wp_rd.meta_value,'%m/%d/%Y' ) $order, " . $pieces[ 'orderby' ];

// 				break;

// 		}

// 	}
// 	return $pieces;
// }

// We don't need to include the Bulk Edit example here because our CPT logic covers this. 
?>