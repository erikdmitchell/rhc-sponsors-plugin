<?php
/**
 * Plugin Name: RHC Sponsors
 * Plugin URI:	https://redhoocrit.com/sponsors
 * Description: A sponsors plugin for RHC. Displays sponsor page, inline styles and carousel through shortcodes.
 * Version:     0.3.1
 * Author:      Long Tail Creative
 * Author URI:  http://longtailcreative.com
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Domain Path: /languages
 * Text Domain: ltc-sponsorss
 *
 * @package RHC_Sponsor_Plugin
 * @category Core
 * @author Matty
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Returns the main instance of RHC_Sponsor_Plugin to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object RHC_Sponsor_Plugin
 */
function RHC_Sponsor_Plugin() {
	return RHC_Sponsor_Plugin::instance();
} // End RHC_sponsor_Plugin()

add_action( 'plugins_loaded', 'RHC_Sponsor_Plugin' );

/**
 * Main RHC_Sponsor_Plugin Class
 *
 * @class RHC_sponsor_Plugin
 * @version	1.0.0
 * @since 1.0.0
 * @package	RHC_sponsor_Plugin
 * @author Matty
 */
final class RHC_Sponsor_Plugin {
	/**
	 * RHC_Sponsor_Plugin The single instance of RHC_Sponsor_Plugin.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The token.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $token;

	/**
	 * The version number.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $version;

	/**
	 * The plugin directory URL.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $plugin_url;

	/**
	 * The plugin directory path.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $plugin_path;

	// Post Types - Start
	/**
	 * The post types we're registering.
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $post_types = array();
	// Post Types - End
	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 */
	public function __construct () {
		$this->token 			= 'ltc-sponsors';
		$this->plugin_url 		= plugin_dir_url( __FILE__ );
		$this->plugin_path 		= plugin_dir_path( __FILE__ );
		$this->version 			= '0.3';

		define( 'RHC_SPONSORS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

		// Post Types - Start
		require_once(RHC_SPONSORS_PLUGIN_DIR . 'classes/class-rhc-sponsor-post-type.php' ); // custom post type
		require_once(RHC_SPONSORS_PLUGIN_DIR . 'classes/class-rhc-sponsor-taxonomy.php' ); // taxonomy
		
		// CUSTOM TEMPLATES CALLED 
		if( ! class_exists( 'Gamajo_Template_Loader' ) ) {
			require RHC_SPONSORS_PLUGIN_DIR . 'includes/class-gamajo-template-loader.php'; // base template class loader for next file
			}
		require RHC_SPONSORS_PLUGIN_DIR . 'includes/class-rhc-sponsors-template-loader.php'; // our custom template class
		
		// CUSTOM SHORTCODE 
		require_once(RHC_SPONSORS_PLUGIN_DIR . 'classes/rhc-sponsor-shortcode.php'); // shortcode
		
		// FUNCTIONS 
		require_once(RHC_SPONSORS_PLUGIN_DIR . 'functions.php'); // generic functions for plugin


		//require_once(RHC_SPONSORS_PLUGIN_DIR . 'admin.php'); // admin columns, interface tweaks, etc
		require_once(RHC_SPONSORS_PLUGIN_DIR . 'metaboxes.php'); // for our custom post type

		// Register an example post type. To register other post types, duplicate this line.
		$this->post_types['ltc-sponsors'] = new RHC_Sponsor_Plugin_Post_Type( 'ltc-sponsors', __( 'Sponsor', 'ltc-sponsors' ), __( 'Sponsors', 'ltc-sponsors' ), array( 'menu_icon' => 'dashicons-smiley' ) );
		// Post Types - End
		register_activation_hook( __FILE__, array( $this, 'install' ) );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
	} // End __construct()

	/**
	 * Main RHC_sponsor_Plugin Instance
	 *
	 * Ensures only one instance of RHC_sponsor_Plugin is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see RHC_sponsor_Plugin()
	 * @return Main RHC_sponsor_Plugin instance
	 */
	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()

	/**
	 * Load the localisation file.
	 * @access  public
	 * @since   1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'ltc-sponsors', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	} // End load_plugin_textdomain()

	/**
	 * Cloning is forbidden.
	 * @access public
	 * @since 1.0.0
	 */
	public function __clone () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 * @access public
	 * @since 1.0.0
	 */
	public function __wakeup () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	} // End __wakeup()

	/**
	 * Installation. Runs on activation.
	 * @access  public
	 * @since   1.0.0
	 */
	public function install () {
		$this->_log_version_number();
	} // End install()

	/**
	 * Log the plugin version number.
	 * @access  private
	 * @since   1.0.0
	 */
	private function _log_version_number () {
		// Log the version number.
		update_option( $this->token . '-version', $this->version );
	} // End _log_version_number()
} // End Class

?>